import random
import threading
import time

hashi = list()
for i in range(5):
    hashi.append(threading.Semaphore(1))

filosofos = ('Pitón', 'Aristóteles', 'Olavão', 'Pondé', 'Karnal')


def filosofo(f):
    f = int(f)
    while True:
        # hashi da esquerda
        hashi[f].acquire()

        # hashi da direita
        hashi[(f + 1) % 5].acquire()

        print(filosofos[f] + " está comendo...")
        time.sleep(random.randint(1, 5))
        hashi[f].release()
        hashi[(f + 1) % 5].release()
        print(filosofos[f] + " está pensando...")
        time.sleep(random.randint(1, 10))


for i in range(5):
    print("Filósofo " + filosofos[i])
    threading._start_new_thread(filosofo, tuple([i]))

while True:
    pass
